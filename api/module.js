var Api = function(path, express,session, cookieParser, bodyParser, config, routeQuestion, routeUser, passport, FacebookStrategy,multipart) {
	var instance;

	function init() {

		var app = express();

		app.set('port', 3005);
		app.use(cookieParser());
		app.use(bodyParser({limit:'50mb'}));
		app.use(bodyParser.json());

		var multipartMiddleware = multipart();

		app.use(session({
			secret:"InSamsungWeTrust"
		}));

		app.use('/images', express.static(path.join(__dirname, '..', '..','images')));
		app.use(express.static(path.join(__dirname, '../public')));


		app.use(passport.initialize());
		app.use(passport.session());

		passport.serializeUser(function(user, done) {
			done(null, user);
		});

		passport.deserializeUser(function(user, done) {
			done(null, user);
		});

		// -- Facebook callback for drawing section
		passport.use('facebook',new FacebookStrategy({
			//mobworkz.info for Android Test

			clientID :'1488239981472596',
		    clientSecret :'41c29fbb721c2751d233ec68c56c60e4',
			callbackURL :'http://mobworkz.info/auth/facebook/callback',	

			// mobworkz.net

			//clientID: '1619511758316953',
			//	clientSecret: '10af3038ec3536c80a286f1ccce10552',
			//	callbackURL: 'http://mobworkz.net/auth/facebook/callback',

			//localhost
				// clientID: '1719469974941807',
				// clientSecret: '86dfefcab81177a23bf7e0a904094a08',
				// callbackURL: 'http://localhost:3000/auth/facebook/callback',
				profileFields: ['id', 'displayName', 'photos','email']
			},
			function(req, accessToken, refreshToken, profile, done) {

				return done(null, profile);

			}
		));

		// -- Facebook callback for scoreboard section
		passport.use('facebook-scoreboard',new FacebookStrategy({
			// mobworkz.net
			clientID: '816681711764215',
				clientSecret: '494ea7ff18dd8e1829b004879c10d433',
				callbackURL: 'http://mobworkz.net/auth/scoreboard-facebook/callback',
				profileFields: ['id', 'displayName', 'photos','email']
			},
			function(req, accessToken, refreshToken, profile, done) {

				return done(null, profile);

			}
		));

		//Question endpoints
		app.get('/getQuestions', routeQuestion.getQuestions);
		app.get('/getDrawingSuggestion',routeQuestion.getDrawingSuggestion);
		app.post('/postResult', routeQuestion.postResult);



		//User endpoints
		app.get('/getList', routeUser.getList);
		app.post('/postDrawing', routeUser.ensureAuthenticated, multipartMiddleware,routeQuestion.postDrawing);

		//Authentication for GAME endpoints
		app.get('/auth/facebook',
			passport.authenticate('facebook',{
				scope : ['email']}
				));

		app.get('/auth/facebook/callback',
			passport.authenticate('facebook', {
				failureRedirect: '/auth/facebook'
			}),
			function(req, res) {

				var name = req.user.displayName.replace(/" "/g,"**0**")
				          .replace(/ç/g,"--1--")
				          .replace(/Ç/g,"--2--")
				          .replace(/ğ/g,"--3--")
				          .replace(/Ğ/g,"--4--")
				          .replace(/ı/g,"--5--")
				          .replace(/İ/g,"--6--")
				          .replace(/ö/g,"--7--")
				          .replace(/Ö/g,"--8--")
				          .replace(/ş/g,"--9--")
				          .replace(/Ş/g,"--10--")
				          .replace(/ü/g,"--11--")
				          .replace(/Ü/g,"--12--");

				if (req.user.hasOwnProperty("emails") == true) {

					res.redirect('/drawing.html' 	 
					 + '?'
					 + 'name=' + name
					 + '&fbID=' + req.user.emails[0].value
					 + '&fbPhoto=' + req.user.photos[0].value);

				} else {

					res.redirect('/drawing.html' 	 
					 + '?'
					 + 'name=' + name
					 + '&fbID=' + req.user.photos[0].value
					 + '&fbPhoto=' + req.user.photos[0].value);
				}         


				

			}
		);

		//Authentication for SCOREBOARD endpoints
		app.get('/auth/scoreboard-facebook',
			passport.authenticate('facebook-scoreboard',{
				scope : ['email']}));

		app.get('/auth/scoreboard-facebook/callback',
			passport.authenticate('facebook-scoreboard', {
				failureRedirect: '/auth/scoreboard-facebook'
			}),
			function(req, res) {
console.log('---- facebook callback');
console.log(req.user);
				if (req.user.hasOwnProperty("emails") == true) {

					res.redirect('/scoreboard.html'+ '?'+ '&fbID=' + req.user.emails[0].value);
				} else {
					res.redirect('/scoreboard.html'+ '?'+ '&fbID=' + req.user.photos[0].value);
				}

			}
		);

		// -- scoreboard auth endpoint
		app.get('/scoreboard-auth', function (req, res) {
			res.sendFile(path.join(__dirname, '../public', 'scoreboard-auth.html'));
		})

		// -- contest endpoint
		app.get('/contest', function (req, res) {
			res.sendFile(path.join(__dirname, '../public', 'index.html'));
		});

		// -- scoreboard endpoint
		app.get('/scoreboard', function (req, res){
			res.sendFile(path.join(__dirname, '../public', 'scoreboard.html'));
		});

		//-- contest after facebook auth.
		app.get('/drawing', function (req, res){
			res.sendFile(path.join(__dirname, '../public', 'drawing.html'));
		});



		return app;
	};

	return {
		getInstance: function() {
			if (!instance) {
				instance = init();
			}

			return instance;
		}
	};
};

module.exports = exports = Api;
