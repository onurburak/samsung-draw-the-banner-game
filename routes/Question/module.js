var RouteQuestion = function(Q, async, store,utils,path,fs) {

  var instance;

  function init() {

    return {
      getQuestions: function(req, res) {
        var query = {
          'status':'APPROVED'
        };
        var options = {
          'sort': {
            'impression': 1
          },
          'limit': 3
        };

        store.find('approvedQuestions', query, options).then(function (questions) {

          res.send({code: 100, message: 'SUCCESS', data: questions});
          return;

        }).fail(function (error) {

          res.send({code: 500, message: 'FAIL_SYSTEM', data: error});
          return;

        });
      },

      postResult: function(req,res) {

        store.findOne('approvedQuestions', {
          _id: utils.toObjectID(req.body._id)
        }).then(function(question) {
          question.impression += 1;
          if (req.body.result == "true") {
            question.score += 1;
          }

            return question;
        }).then(function(question){
          return store.update('approvedQuestions', {'_id': utils.toObjectID(question._id)},question);
        }).then(function(question){
          var ID = req.body.fbID;
          return store.findOne('audience',{fbID:ID});
          
        }).then(function(user){
          console.log(user);
          user.impression +=1
          if (req.body.result == "true") {
            user.score += 1;
          }
            return store.update('audience',{'_id':utils.toObjectID(user._id)},user);
        }).then(function(user){
          res.send({code:100, message:'SUCCESS'});
          return;
        }).fail(function(error){
          res.send({code:error.code, message:error.message, data:error.data});
          return;
        });
      },

      postDrawing : function(req,res){

          var dataUrl = req.body.drawing;
  
          //-- write file

          var dataString = dataUrl.split( "," )[ 1 ];
          var buffer = new Buffer( dataString, 'base64');
          var extension = dataUrl.match(/\/(.*)\;/)[ 1 ];
          var fullFileName = new Date().getTime() + "." + extension;
          fs.writeFileSync(path.join(__dirname, '..', '..','..','images', fullFileName), buffer, "binary" );


          var drawing = {
            fbID: req.body.fbID
            ,photoPath: "/images/" + fullFileName
            ,shape: req.body.shape
            ,status: "UNAPPROVED"
            ,name: req.body.name
            ,fbPhoto: req.body.fbPhoto
            ,msg: req.body.msg
          };

          store.save('unapprovedQuestions',drawing).then(function(drawing){
            res.send({code:200,message:"SUCCESS"});
          }).fail(function(err){
            res.send({code:500,message:'FAIL_SYSTEM',data:err});
          });
       
      },
      getDrawingSuggestion: function(req,res){

        var query={};

        store.count('suggestions',query).then(function(total){
          var random=Math.floor(Math.random()*total);
          var options={
            'skip':random,
            'limit':1
          };
          return store.find('suggestions',query,options);
        }).then(function(data){

          res.send({code:200,message:'SUCCESS',data:data});
          return;
        }).fail(function(err){
          res.send({code:500,message:'FAIL_SYSTEM',data:err});
          return;

        })
      }

    };
  }

  return {

    getInstance: function() {

      if (!instance) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = RouteQuestion;