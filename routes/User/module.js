var RouteUser = function(Q, async, store,utils,passport) {

  var instance;

  function init() {

    return {

      getList: function(req,res){

        var query = {
        };
        var options = {
          'sort': {
            'score': -1
          },
          'limit': 20
        };

        var data={};
        var tempUser={};

        var id= req.query.fbId;

        store.find('audience', query, options).then(function (users) {

          var queryForUser = {
            fbID:''
          };

          queryForUser.fbID=id;
          data.users=users;
          return store.findOne('audience',queryForUser);

        }).then(function(user){

          console.log(user);
          if(user!=null){

            tempUser=user;
            var score=user.score;
            var tempQuery={'score':{ $gte : score  }};
            return store.count('audience',tempQuery);

          } else{
              return 0;
          }
            
        }).then(function(count){

          if(count>20){
            tempUser.index=count;
            data.user=tempUser;
          }

          data.person = tempUser;   

          res.send({code: 200, message: 'SUCCESS', data: data});
          return;
        }).fail(function (error) {

          res.send({code: 500, message: 'FAIL_SYSTEM', data: error});
          return;

        });
      },

      ensureAuthenticated: function(req,res,next){
        if (req.isAuthenticated()) { return next(); }
          res.send({code:500,message:"FAIL_AUTH"});
        return;
      }
      

    };
  }

  return {

    getInstance: function() {

      if (!instance) {
        instance = init();
      }

      return instance;
    }

  };

};

module.exports = exports = RouteUser;