require('rootpath')();
var http = require('http'),
	path = require('path'),
	session = require('express-session'),
	express = require('express'),
	Q = require('q'),
	async = require('async'),
	_ = require('underscore'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	mongoSkin = require('mongoskin'),
	passport = require('passport'),
	FacebookStrategy = require('passport-facebook').Strategy,
	multipart = require('connect-multiparty'),
	fs = require('fs');




var config = require(path.join('lib', 'modules', 'Config')).getConfig();
config.set('rootPath', __dirname);
config.set('rootUrl', 'http://127.0.0.1:3000');


var mongoConfig = require(path.join('lib', 'modules', 'Config')).getConfig();
mongoConfig.load({
	mongoAddr: ('mobworkz.net:27017'/* 'localhost:27017'*/),
	mongoDb: 'samsung',
	mongoRs: (process.env.MONGO_RS || '')
});


var MongoDbFactory = require(path.join('lib', 'modules', 'MongoDbFactory'));
var mongoDbFactory = new MongoDbFactory();
var mongoDb = mongoDbFactory.getDb(mongoSkin, mongoConfig);
var StoreFactory = require(path.join('lib', 'modules', 'StoreFactory'));
var storeFactory = new StoreFactory();
var store = storeFactory.getStore(Q, async, _, mongoDb);


var utils = require(path.join('lib', 'modules' ,'Utils'))(mongoSkin).getInstance();


// Route Section
var routeQuestion = require(path.join('routes', 'Question'))(Q,async,store,utils,path,fs).getInstance();
var routeUser = require(path.join('routes', 'User'))(Q,async,store,utils,passport).getInstance();






var app = require(path.join('api'))(path,express,session,cookieParser,bodyParser,config,routeQuestion,routeUser,passport,FacebookStrategy,multipart).getInstance();

http.createServer(app).listen(app.get('port'), function(){
	console.log('App listens port '+ app.get('port'));
});
