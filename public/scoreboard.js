
// -- method to get query string parameter
function getParameterFromUrl (name) {
  
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
     return null;
  }
  else{
     return results[1] || 0;
  }
}

// -- document ready
$(document).ready(function(){

	var fbId = getParameterFromUrl("fbID");//-- get facebook id

	// -- fetch scoreboard data
	$.ajax({
		method: "GET",
		url: "http://mobworkz.info/getList?fbId=" + fbId,
	}).success(function(result) {
		var temp=result.data.users;
		var isUserInList = result.data.hasOwnProperty("user");
		var isPersonInList = jQuery.isEmptyObject(result.data.person);

		console.log(isPersonInList);

		temp.forEach(function(data,i){
			var name=data.name;
			var score=data.score;
			var photo=data.fbPhoto;
			var html="<tr><td class='order'>"+(i+1)+".</td><td class='img'><div><img src='"+photo+"'></div></td><td class='name'>"+name+"</td><td class='score'>"+score+"<i class='fa fa-check'></i></td></tr>";
			$('#scoreboardTable').append(html);
		});

		if (isUserInList) {
			var user = result.data.user;
			var html="<tr><td class='order'>"+(user.index)+".</td><td class='img'><div><img src='"+user.fbPhoto+"'></div></td><td class='name'>"+user.name+"</td><td class='score'>"+user.score+"<i class='fa fa-check'></i></td></tr>";
			$('#scoreboardTable').append(html);
		}

		if(isPersonInList){
			$('a#show-yourself').hide();
		}

		// -- set user to impression table
		var person = result.data.person;
		var personHTML = "<tr><td class='img'><div><img src='"+person.fbPhoto+"'></div></td><td class='name'>"+person.name+"</td><td class='score'>"+person.score+"</td><td class='score'>"+person.impression+"</td></tr>";
		$("#impressionTbl").append(personHTML);

		console.log(result);
	});


	// -- fetch

	// -- navigation method
	$('.header a.navigator').click(function(event) {
		event.preventDefault();
		var target = $(this).data('location');

		$('.part').addClass('hide');
		$(target).removeClass('hide');
		$('.header a.navigator').removeClass('hide')
		$(this).addClass('hide');
	});
});