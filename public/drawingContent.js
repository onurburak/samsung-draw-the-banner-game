//-- variables
var postObj = {
     fbID:''
    ,shape:''
    ,name:''
    ,fbPhoto:''
    ,drawing:''
    ,msg:''
  };


// -- method to go to next frame
function goToNext (next, previous) {


  $(".frame" + previous).addClass("hide");
  $(".frame" + next).removeClass("hide");

  if (next == 2) {changeQuestion();}

  // if($(".frame" + next).hasClass('auto-next')){
  //   setTimeout(function(){goToNext(5,4);}, 3000);
  // }

}

//-- save drawing
function sendDrawing () {


  postObj.fbID= getParameterFromUrl("fbID");
  postObj.name= getParameterFromUrl("name")
          .replace(/%20/g," ")
          .replace(/--0--/g,"ç")
          .replace(/--1--/,"ç")
          .replace(/--2--/g,"ğ")
          .replace(/--3--/g,"ğ")
          .replace(/--4--/g,"ı")
          .replace(/--5--/g,"ı")
          .replace(/--6--/g,"ö")
          .replace(/--7--/g,"ö")
          .replace(/--8--/g,"ş")
          .replace(/--9--/g,"ş")
          .replace(/--10--/g,"ü")
          .replace(/--11--/g,"ü");




  postObj.shape= getParameterFromUrl("shape");
  postObj.fbPhoto= getParameterFromUrl("fbPhoto") 
            + "&oe=" + getParameterFromUrl("oe")
            + "&__gda__=" + getParameterFromUrl("__gda__");

  //-- get canvas as image file            
  var pic = document.getElementById('simple_sketch');
  postObj.drawing = pic.toDataURL();
  // --get shape
  postObj.shape = $("#drawing-shape").text();

  postObj.msg=document.getElementById('message').value;

  //-- send req
  $.ajax({
      type: "POST",
      url: "http://mobworkz.info/postDrawing",
      data: postObj,
      cache: false,
      contentType: "application/x-www-form-urlencoded",
      success: function (body) {
        
      }
    });



  //-- go to finishing screen
  goToNext(6, 5);
}

//-- get params from url
function getParameterFromUrl (name) {
  
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
     return null;
  }
  else{
     return results[1] || 0;
  }

}

//-- get shape

function getShape () {

    $.ajax({
      type: "GET",
      url: "http://mobworkz.info/getDrawingSuggestion",
      cache: false,
      contentType: "application/json",
      success: function (body) {
       $("#drawing-shape").html(body.data[0].word);
      }
    });
}

$(document).ready(function () {
    getShape();
});
