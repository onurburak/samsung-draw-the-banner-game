/*
* Created at 18 Sept.
*/

var  questions= []
	,questionKey = -1
	,rightAnswerCount = 0
	,wrongAnswerCount = 0
	,currentFrame = 0;

// -- method to go to next frame
function goToNext (next, previous) {

	if (next == 2) {changeQuestion();}

	$(".frame" + previous).addClass("hide");
	$(".frame" + next).removeClass("hide");

	
}

// -- fetch questions from rest
function fetchQuestions () {

	$.getJSON("http://mobworkz.info/getQuestions", function(response){
		var responseData = response.data;
		for(var key in responseData){
			questions.push({
				_id: responseData[key]._id
				,fbID: responseData[key].fbID
				,image:responseData[key].photoPath
				,shape: responseData[key].shape
				,alternativeOption: responseData[key].alternativeOption
			});
		}
	});

}

//-- change question
function changeQuestion () {


	questionKey++;
alert(questionKey);
alert(questions[questionKey].image);
	var rand = Math.floor((Math.random() * 10) + 1);//-- get random int

	$("#questionPicture").attr("src", "http://mobworkz.info"+questions[questionKey].image);//-- set question image
	//-- random 
	if (rand % 2 == 0) {
		$("#questionOption1").text(questions[questionKey].shape);
		$("#questionOption2").text(questions[questionKey].alternativeOption);
	} else {
		$("#questionOption2").text(questions[questionKey].shape);
		$("#questionOption1").text(questions[questionKey].alternativeOption);
	}

}

//-- validate answer
function validateAnswer (pressed, other) {

	// -- change color of buttons
	if (pressed.innerText == questions[questionKey].shape) {
		pressed.setAttribute("class","button success");
		rightAnswerCount++;

		scoreQuestion({
			 _id: questions[questionKey]._id
			,result: true
			,fbID: questions[questionKey].fbID
		});

	} else {
		pressed.setAttribute("class","button error");
		other.setAttribute("class","button success");
		wrongAnswerCount++;

		scoreQuestion({
			 _id: questions[questionKey]._id
			,result: false
			,fbID: questions[questionKey].fbID
		});
	}
	
	// -- change question, clear buttons and chech for last screen
	setTimeout(function(){

		if (questions.length == questionKey + 1) {

			if (rightAnswerCount > 1) {
				goToNext(3,2);
				currentFrame = 3;
			}
			else{
				goToNext(4,2);
				currentFrame = 4;
			}

		} else {

			changeQuestion(); 
			pressed.setAttribute("class","button");
			other.setAttribute("class","button");
		}

	}, 1000);

}

//-- score answer on back-end
function scoreQuestion (obj) {
	
  	$.ajax({
      type: "POST",
      url: "http://mobworkz.info/postResult",
      data: obj,
      cache: false,
      success: function (body) {

      }
    });
}

//-- fb authentication
function authenticateWithFB () {
	window.location.assign("http://mobworkz.info/auth/facebook");
}

$(document).ready(function(){
	fetchQuestions();
});


